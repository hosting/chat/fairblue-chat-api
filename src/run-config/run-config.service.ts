import { Injectable } from '@nestjs/common';
import {FairblueMeetingConfig} from "../fairblue/interfaces/FairblueMeetingConfig.interface";

@Injectable()
export class RunConfigService {
    fairblueMeetingConfig: FairblueMeetingConfig;
    
    updateFairblueMeetingConfig(pConfig: FairblueMeetingConfig) {
        this.fairblueMeetingConfig = pConfig;
    }
}
