import {Injectable} from '@nestjs/common';
import * as sdk from "matrix-js-sdk";
import {ConfigService} from "@nestjs/config";
import {FairblueService} from "../fairblue/fairblue.service";

const matrixcs = require("matrix-js-sdk/lib/matrix");
const request = require("request");
matrixcs.request(request);

@Injectable()
export class MatrixService  {
    client: any;

    constructor(
        private readonly fairblue: FairblueService,
        private configService: ConfigService
    ) {

    }

    parseUsername(tUser: string): string {
        return (tUser.split(":"))[0].substring(1) || "";
    }

    async connect() {
        try {
            this.client = sdk.createClient(this.configService.get("MATRIX_SERVER"));
            let loginResponse = await this.client.login("m.login.password", {
                "user": this.configService.get("MATRIX_USER"),
                "password": this.configService.get("MATRIX_PASSWORD")
            });
            await this.client.startClient();
        } catch (e) {
            console.error(`#@# error: ${e}`);
        }
    }

    subscribeToRoomEvents(pCallback) {
        try {
            let syncState = this.client.once('sync', async (state, prevState, res) => {
                // @ts-ignore
                this.client.on("Room.timeline", pCallback);
            });
        } catch (e) {
            console.error(`#@# error: ${e}`);
        }
    }
    
    sendMessage(pMsg) {
        let roomId = this.configService.get("MATRIX_ROOM")

        var content = {
            "body": pMsg,
            "msgtype": "m.text",
            "origin": "api" 
        };

        this.client.sendEvent(roomId, "m.room.message", content, "").then((res) => {
            // message sent successfully
        }).catch((err) => {
            console.log(err);
        });
    }



}
