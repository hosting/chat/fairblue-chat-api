import {Injectable} from '@nestjs/common';
import {Subject} from "rxjs";
import {ParsedMessage} from "../fairblue/interfaces/ParsedMessage.interface";
import {MatrixMessage} from "../matrix/MatrixMessage.interface";

@Injectable()
export class EventBroker {
    fairblueMeetingRegistered: Subject<boolean> = new Subject<boolean>();
    fairblueMessageReceived: Subject<ParsedMessage> = new Subject<ParsedMessage>();
    matrixMessageReceived: Subject<MatrixMessage> = new Subject<MatrixMessage>();

}
