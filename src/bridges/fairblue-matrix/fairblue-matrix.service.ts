import {Injectable, OnModuleInit} from '@nestjs/common';
import {FairblueService} from "../../fairblue/fairblue.service";
import {MatrixService} from "../../matrix/matrix.service";
import {ConfigService} from "@nestjs/config";
import {EventBroker} from "../../event-broker/event-broker.service";
import {RedisService} from "../../fairblue/redis.service";
import {ParsedMessage} from "../../fairblue/interfaces/ParsedMessage.interface";
import {MatrixMessage} from "../../matrix/MatrixMessage.interface";

@Injectable()
export class FairblueMatrixBridgeService implements OnModuleInit {

    constructor(
        private readonly eventBroker: EventBroker,
        private readonly fairblue: FairblueService,
        private readonly redis: RedisService,
        private readonly matrix: MatrixService,
        private configService: ConfigService
    ) {
    }


    async activate() {

        // connect matrix client
        await this.matrix.connect();

        // callback for handling incoming events 
        let tCallback = (event, room, toStartOfTimeline) => {
            if (room.roomId !== this.configService.get("MATRIX_ROOM") || event.getType() !== "m.room.message") {
                return; // only use messages
            }

            let tMsg: MatrixMessage = {
                sender: event.getSender(),
                content: event.event.content.body,
                origin: event.event.content.origin 
            }
            if(event.event && event.event.content && event.event.content.origin) tMsg.origin = event.event.content.origin

            this.eventBroker.matrixMessageReceived.next(tMsg);
        }

        // hook callback to room event
        await this.matrix.subscribeToRoomEvents(tCallback);

        // if message is received, forward the message to fairblue
        this.eventBroker.matrixMessageReceived.subscribe((message: MatrixMessage) => {
            console.log(`#@# message: `);
            console.log(message);
            if(message.origin !== "api") {
                let tMsg = this.fairblue.buildMessage(`${message.sender ? (this.matrix.parseUsername(message.sender) + ": ") : ""} ${message.content}`);
                if (tMsg) this.fairblue.sendMessage(tMsg);
            }
        });


        // listen to a registration event
        await this.redis.listenForRegistrationCommand();

        // if a meeting is registered, subscribe to chat messages from it
        this.eventBroker.fairblueMeetingRegistered.subscribe(async (val) => await this.redis.subscribeToChatMessages());
        this.eventBroker.fairblueMessageReceived.subscribe(async (val: ParsedMessage) => {
            if(val.body.sender.name !== "fairblue-chat-api")  {
                this.matrix.sendMessage(`${val.body.sender.name}: ${val.body.message}`)
            }
        });
    }

    async deactivate() {

    }

    async onModuleInit(): Promise<void> {
        console.log("Activating Fairblue-Matrix bridge");
        await this.activate();
    }
}
