import {Module} from '@nestjs/common';
import {ChatController} from './chat/chat.controller';
import { MatrixService } from './matrix/matrix.service';
import { RunConfigService } from './run-config/run-config.service';
import {ConfigModule} from "@nestjs/config";
import { FairblueService } from './fairblue/fairblue.service';
import {RedisService} from "./fairblue/redis.service";
import {FairblueMatrixBridgeService} from "./bridges/fairblue-matrix/fairblue-matrix.service";
import {EventBroker} from "./event-broker/event-broker.service";

@Module({
    imports: [ConfigModule.forRoot()],
    controllers: [ChatController],
    providers: [
        RunConfigService,
        EventBroker,
        RedisService,
        FairblueService,
        MatrixService,
        FairblueMatrixBridgeService,
    ],
})
export class AppModule {
    constructor(
        ) {
    }
}
