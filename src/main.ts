import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ValidationPipe} from "@nestjs/common";
import * as fs from "fs";

const httpsOptions = {
  key: fs.readFileSync('./certs/key.pem'),
  cert: fs.readFileSync('./certs/cert.crt'),
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    httpsOptions,
  });
  // TODO: swagger module (https://docs.nestjs.com/openapi/introduction)
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();