import {Body, Controller, Param, Post} from '@nestjs/common';
import {ChatMessageBody} from "./ChatMessageBody";
import {FairblueService} from "../fairblue/fairblue.service";

@Controller('chat')
export class ChatController {

    constructor(private readonly fairblue: FairblueService) {

    }

    // @Post('send')
    // async sendMessage(
    //     @Param() params,
    //     @Body() body: ChatMessageBody
    // ): Promise<string> {
    //     let a = await this.fairblue.sendMessage(body);
    //     return Promise.resolve(JSON.stringify(a));
    // }
}
