import {Injectable} from '@nestjs/common';
import {createClient, RedisClientType, RedisDefaultModules, RedisModules, RedisScripts} from "redis";
import {RedisConfig} from "./interfaces/RedisConfig.interface";
import {RedisChatMessage} from "./interfaces/RedisChatMessage.interface";
import {ChatMessageBody} from "../chat/ChatMessageBody";
import {RunConfigService} from "../run-config/run-config.service";
import {FairblueMeetingConfig} from "./interfaces/FairblueMeetingConfig.interface";
import {ParsedMessage} from "./interfaces/ParsedMessage.interface";
import {EventBroker} from "../event-broker/event-broker.service";

@Injectable()
export class RedisService {
    publisher: RedisClientType<RedisDefaultModules & RedisModules, RedisScripts> = null;
    subscriber: RedisClientType<RedisDefaultModules & RedisModules, RedisScripts> = null;
    config: RedisConfig;

    constructor(
        private rcf: RunConfigService,
        private eventBroker: EventBroker
    ) {
        this.publisher = createClient();
        this.subscriber = this.publisher.duplicate();
        this.publisher.connect();
        this.subscriber.connect();

        this.config = {
            channel: "to-akka-apps-redis-channel",
            chatId: "MAIN-PUBLIC-GROUP-CHAT",
            eventName: "SendGroupChatMessageMsg"
        }
    }

    private __buildMessage(pBody: ChatMessageBody): RedisChatMessage {
        let tTimestamp = Date.now();
        let tMessage: RedisChatMessage = {
            envelope: {
                name: this.config.eventName,
                routing: {
                    meetingId: pBody.meetingId,
                    userId: pBody.userId
                },
                timestamp: tTimestamp,
            },
            core: {
                header: {
                    name: this.config.eventName,
                    meetingId: pBody.meetingId,
                    userId: pBody.userId
                },
                body: {
                    msg: {
                        color: pBody.color || "#000",
                        correlationId: pBody.userId + "-" + tTimestamp,
                        sender: {id: pBody.userId, name: "fairblue-chat-api"},
                        message: pBody.msg
                    },
                    chatId: this.config.chatId,
                }
            }
        };
        return tMessage;
    }

    private __checkForRegisterCommand(pMessage: ParsedMessage): boolean {
        return (
            pMessage.hasOwnProperty("body")
            && pMessage.body.hasOwnProperty("message")
            && pMessage.body.message.substring(0, 9) === "/register"
        )
    }

    private __parseMessage(pMessage: string): ParsedMessage {
        let tObj = JSON.parse(pMessage);
        if (
            tObj.hasOwnProperty("envelope")
            && tObj.envelope.hasOwnProperty("name")
            && tObj.envelope.name === "SendGroupChatMessageMsg"
            && tObj.hasOwnProperty("core")
            && tObj.core.hasOwnProperty("header")
            && tObj.core.header.hasOwnProperty("name")
            && tObj.core.header.hasOwnProperty("meetingId")
            && tObj.core.header.hasOwnProperty("userId")
            && tObj.core.header.name === "SendGroupChatMessageMsg"
            && tObj.core.hasOwnProperty("body")
            && tObj.core.body.hasOwnProperty("msg")
        ) {
            return <ParsedMessage>{
                body: tObj.core.body.msg,
                meetingId: tObj.core.header.meetingId,
                userId: tObj.core.header.userId
            };
        }
    }

    async sendMsg(pMsg: ChatMessageBody) {
        const tMsg: RedisChatMessage = this.__buildMessage(pMsg);
        let a = await this.publisher.publish(this.config.channel, JSON.stringify(tMsg));
        return a;
    }

    async listenForRegistrationCommand() {
        
        await this.subscriber.subscribe(this.config.channel, async (message) => {
            let tP = this.__parseMessage(message);
            let tReg = (tP) ? this.__checkForRegisterCommand(tP) : false;
            if (tReg) {
                let tMeetingConfig: FairblueMeetingConfig = {
                    meetingId: tP.meetingId,
                    userId: tP.userId
                }
                this.rcf.updateFairblueMeetingConfig(tMeetingConfig);
                this.eventBroker.fairblueMeetingRegistered.next(true);
            }
        });
    }

    async subscribeToChatMessages() {
        if (!this.rcf.fairblueMeetingConfig || !this.rcf.fairblueMeetingConfig.meetingId) {
            console.error("can only subscribe to chat as soon as a meeting has been registered")
            return;
        }
        await this.subscriber.unsubscribe(this.config.channel); 
        await this.subscriber.subscribe(this.config.channel, (message) => {
            let tP = this.__parseMessage(message);
            
            if(tP) {
                this.eventBroker.fairblueMessageReceived.next(tP);
            }
        });
    }

}

