export interface RedisConfig {
    channel: string;
    chatId: string;
    eventName: string;
}
