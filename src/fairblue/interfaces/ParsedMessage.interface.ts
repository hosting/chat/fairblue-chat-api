import {RedisChatMessageCoreBodyMsg} from "./RedisChatMessage.interface";

export interface ParsedMessage {
    body: RedisChatMessageCoreBodyMsg;
    meetingId: string;
    userId: string;
}

