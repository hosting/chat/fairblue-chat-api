export interface RedisChatMessage {
    envelope: RedisChatMessageEnvelope;
    core: RedisChatMessageCore;
}

export interface RedisChatMessageEnvelopeRouting {
    meetingId: string;
    userId: string;
}

export interface RedisChatMessageEnvelope {
    name: string;
    routing: RedisChatMessageEnvelopeRouting
    timestamp: number;
}

export interface RedisChatMessageCore {
    header: RedisChatMessageCoreHeader;
    body: RedisChatMessageCoreBody;
}

export interface RedisChatMessageCoreHeader {
    name: string;
    meetingId: string;
    userId: string;
}

export interface RedisChatMessageCoreBody {
    msg: RedisChatMessageCoreBodyMsg;
    chatId: string;
}

export interface RedisChatMessageCoreBodyMsg {
    color: string;
    correlationId: string;
    sender: RedisChatMessageCoreBodyMsgSender;
    message: string;
}

export interface RedisChatMessageCoreBodyMsgSender {
    id: string;
    name: string;
}
