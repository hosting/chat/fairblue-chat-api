export interface FairblueMeetingConfig {
    meetingId: string,
    userId: string,
}