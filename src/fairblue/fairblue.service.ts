import {Injectable, OnModuleInit} from '@nestjs/common';
import {RunConfigService} from "../run-config/run-config.service";
import {ChatMessageBody} from "../chat/ChatMessageBody";
import {RedisService} from "./redis.service";

@Injectable()
export class FairblueService {
    constructor(
        private redis: RedisService,
        private rcf: RunConfigService
    ) {
    }

    sendMessage(pMsg: ChatMessageBody) {
        return this.redis.sendMsg(pMsg);
    }

    buildMessage(pMsg): ChatMessageBody {
        if (!this.rcf.fairblueMeetingConfig || !this.rcf.fairblueMeetingConfig.meetingId || !this.rcf.fairblueMeetingConfig.userId) {
            console.error("no meetingId or userId configured yet. type /register in the fairblue meeting that you'd like to " +
                "register");
            return;
        }
        return <ChatMessageBody>{
            meetingId: this.rcf.fairblueMeetingConfig.meetingId,
            userId: this.rcf.fairblueMeetingConfig.userId,
            username: "",
            msg: pMsg,
            color: "#000"
        }
    }
}
