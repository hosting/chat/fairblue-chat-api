const data = JSON.stringify({
  "meetingId": "",
  "userId": "",
  "username": "",
  "msg": "Hello World!",
  "color": "#888"
});

const xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://dev.fairteaching.net:3000/chat/send");
xhr.setRequestHeader("Content-Type", "application/json");

xhr.send(data);